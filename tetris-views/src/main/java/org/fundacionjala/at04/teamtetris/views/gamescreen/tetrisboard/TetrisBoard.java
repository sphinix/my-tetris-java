package org.fundacionjala.at04.teamtetris.views.gamescreen.tetrisboard;

/**
 * Created by AbelBarrientos on 6/28/2017.
 */
public interface TetrisBoard {

  TetrisBoardView getTetrisPane();

  void drawTetrisBoard();
}
