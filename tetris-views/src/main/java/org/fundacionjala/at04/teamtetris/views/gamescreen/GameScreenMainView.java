package org.fundacionjala.at04.teamtetris.views.gamescreen;


/**
 * Created by AbelBarrientos on 6/28/2017.
 */
public interface GameScreenMainView {

  GameScreenView drawGameScreenView();
}
