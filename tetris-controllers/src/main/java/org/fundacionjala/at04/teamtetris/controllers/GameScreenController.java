package org.fundacionjala.at04.teamtetris.controllers;

import javafx.scene.layout.BorderPane;
import org.fundacionjala.at04.teamtetris.models.BoardModel;
import org.fundacionjala.at04.teamtetris.views.gamescreen.GameScreenMainView;

/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class GameScreenController {

  private BoardModel model;
  private GameScreenMainView view;

  /**
   * Default Constructor
   *
   * @param model BoardModel.
   * @param view  BoardView.
   */
  public GameScreenController(BoardModel model, GameScreenMainView view) {
    this.model = model;
    this.view = view;
    this.model.getBoard();
  }

  public BorderPane drawGameScreen() {
    return view.drawGameScreenView();
  }
}
