package org.fundacionjala.at04.teamtetris.controllers;

import javafx.stage.Stage;
import org.fundacionjala.at04.teamtetris.controllers.eventhandlers.ExitEventHandler;
import org.fundacionjala.at04.teamtetris.models.StageModel;
import org.fundacionjala.at04.teamtetris.views.StageView;

/**
 * Created by abelb on 6/26/2017.
 */
public class MainStageController {

  private StageView view;
  private StageModel model;

  /**
   * Default Construtor.
   * @param view StageView.
   * @param model StageModel.
   */
  public MainStageController(StageView view, StageModel model) {
    this.view = view;
    this.model = model;
  }

  /**
   * Set Stage Title.
   */
  private void setStageTitle() {
    view.getMainView().setTitle(model.getTitle());
  }

  /**
   * Set up the Event Handlers.
   */
  private void setupEventHandlers() {
    Stage stage = view.getMainView();
    stage.setOnCloseRequest(new ExitEventHandler());
  }

  /**
   * Run the events.
   */
  public void run() {
    setupEventHandlers();
    setStageTitle();
  }
}
