package org.fundacionjala.at04.teamtetris.models;


/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public interface BoardModel {

  int[][] getBoard();
}
