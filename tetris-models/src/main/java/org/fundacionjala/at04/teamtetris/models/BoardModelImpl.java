package org.fundacionjala.at04.teamtetris.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class BoardModelImpl implements BoardModel {

  private final int rows;
  private final int columns;
  private List<int [][]> boardGame;
  private int[][] board;

  /**
   * Default Constructor.
   * @param rows Integer.
   * @param columns Integer.
   */
  public BoardModelImpl(int rows, int columns) {
    this.rows = rows;
    this.columns = columns;
    board = new int[this.rows][this.columns];
    boardGame = new ArrayList<>();
    boardGame.add(board);
  }

  /**
   * Return the Board matrix.
   * @return Integer[][].
   */
  @Override
  public int[][] getBoard() {
    int[][] newBoard = board;
    return newBoard;
  }
}
